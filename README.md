# Minimal jetpack compose android app
This code tries to show a simple hello world sample app which is as small as possible.

Following steps are taken to reach this:
- The MainActivity extends from ComponentActivity instead of AppCompatActivity (To avoid including AppCompat libs)
- For release builds "minifyEnabled true" is set.
- Removed any androix library dependencies except androidx.ui
- Avoid using kotlin reflection (Would increase the size by about 800 kb)

Build the release apk to check the size of the apk.
